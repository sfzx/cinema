require('dotenv').config()
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const morgan = require('morgan')
const { handleError } = require('./app/helpers/error')

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
const db = mongoose.connection
db.on('error', error => console.error(error))
db.once('open', () => console.log('Connected to Mongoose'))

app.use(morgan('dev'))
app.use(bodyParser({ limit: '10mb' }))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: 'false' }))

app.use('/api/', require('./app/cinema/cinema.router'))
app.use('/api/auth', require('./app/auth/auth.router'))

app.use((err, req, res, next) => {
  handleError(err, res)
})

module.exports = app
