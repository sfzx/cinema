require('dotenv').config()
const { Router } = require('express')
const validation = require('./auth.validation')
const authService = require('./auth.service')

const router = Router()

// /api/auth/register
router.post('/register', validation.register, async (req, res) => {
  await authService.createUser(req.body)
  res.status(201).json({ message: 'User created' })
})

// /api/auth/login
router.post('/login', validation.login, async (req, res) => {
  const data = await authService.loginUser(req.body)
  res.status(200).json({ token: data.token, userId: data.userId, message: 'Successful logined' })
})

module.exports = router
