import React, { useState, useEffect } from 'react'
import Done from '@material-ui/icons/Done'
import Input from '@material-ui/icons/Input'
import { Link } from 'react-router-dom'
import { useHttp } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'

export const RegisterPage = () => {
  const message = useMessage()
  const { loading, request, error, clearError } = useHttp()
  const [form, setForm] = useState({
    firstName: '', lastName: '', email: '', password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const registerHandler = async () => {
    try {
      const data = await request('/api/auth/register', 'POST', { ...form })
      message(data.message)
    } catch (e) {}
  }
  return (

    <div className='container-auth animation'>
      <div className='row'>
        <div className='col s12' id='reg-htmlForm'>
          <div className='row'>
            <div className='col s12'>
              <p className='flow-text center-align fonts-styles'>Register</p>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s6'>
              <input id='firstName' name='firstName' type='text' className='validate' value={form.firstName} onChange={changeHandler} required />
              <label htmlFor='firstName'>First Name</label>
            </div>
            <div className='input-field col s6'>
              <input id='lastName' name='lastName' type='text' className='validate' value={form.lastName} onChange={changeHandler} required />
              <label htmlFor='lastName'>Last Name</label>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>
              <input id='email' name='email' type='email' className='validate' value={form.email} onChange={changeHandler} required />
              <label htmlFor='email'>Email</label>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>
              <input id='password' name='password' type='password' className='validate' value={form.password} onChange={changeHandler} minLength='6' required />
              <label htmlFor='password'>Password</label>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>
              <button className='btn btn-large btn-register waves-effect waves-light col s12' onClick={registerHandler} disabled={loading}>Register
                <i className='material-icons dist'><Done /></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <Link to='/auth/login' title='Login' className='ngl btn-floating btn-large waves-effect waves-light red'>
        <i className='material-icons mat-ico'>
          <Input fontSize='large' />
        </i>
      </Link>
    </div>

  )
}
