/* eslint-env jest */
const app = require('../app')
const request = require('supertest')
const cinemaService = require('../app/cinema/cinema.service')

const pondObject = {
  type: 'image/png',
  data: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABHNCSVQICAgIfAhkiAAABGZJREFUeJzt3bFR3FAUQFGth9BUwBDSgGujAAqgNjdAyFABjr0OPC7A+zTzJe45ueDvanXnJ//psjFxXb0AtsvqBZzZt9ULANYRAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAj7CkcpT30k9+f3WYOfXh52Wsn/e3v5GP+NH79+77CSZU7//NgBQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQNjd6gVsCwd6TIdxMLfyHuwwjGSP3+7SoSKeAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAibnkVedpb/n8/Xx5uvfXv52HElt3l6eVi9hKVW3oPpd3///L7TSkZGz7AdAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQJAIQtPw48fT10/Tgt6+xxlHmHV5Q7DgzcRgAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAgTAAg7LItfsX3dB7AxBFmCRzhFeUrrbwHR/jud5gHMGIHAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGF3k4tXDvM4giMMlDi76Xd4hKEuE9NnaDpQpP0EQ5wAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQJgAQNjo9eBHcPbXQ7PO9LfzFV4PbwcAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYQIAYZdt2663Xvz5+rjjUpq+wpnyCfMcZu6f30fX2wFAmABAmABAmABAmABAmABAmABAmABAmABAmABAmABAmABAmABAmABAmABAmABA2N3qBUxNByKw2Inv31cYiGMHAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGGXbduuqxexyvW6/qNfLpfR9Ss/w3Tt23b+9Z+dHQCECQCECQCECQCECQCECQCECQCECQCECQCECQCECQCECQCECQCECQCE3W1/jwTfavl52iMc6eWcpr+dgxwnHi3CDgDCBADCBADCBADCBADCBADCBADCBADCBADCBADCBADCBADCBADCBADCBADCjnCgeXQo2zwAVtlpHsDSZ9AOAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMIEAMJOPxBk9I8NE0nbaaDHlIEgwBoCAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGECAGGHOBA95FA/q5z++bEDgDABgDABgDABgDABgDABgDABgDABgDABgDABgDABgDABgDABgDABgDABgLA/p8tdCq3oHksAAAAASUVORK5CYII='
}

const filmBody = {
  title: 'Title film',
  director: 'By',
  screenWriter: 'Writer',
  description: 'Some film description',
  mainCharacter: 'Character One, Character Two',
  filmDuration: 129,
  realiseDate: '2019-04-22',
  startOfRental: '2019-05-10',
  endOfRental: '2019-05-27',
  cover: pondObject
}

describe('/api', () => {
  beforeEach(async () => {
    await cinemaService.addFilm(filmBody)
  })

  afterEach(async () => {
    await cinemaService.deleteFilm(filmBody.title)
  })

  test('On POST /admin/createfilm should create film', async () => {
    const res = await request(app)
      .post('/api/admin/createfilm')
      .send({
        title: 'Filman Title',
        director: 'Created By',
        screenWriter: 'Writer',
        description: 'Some film description',
        mainCharacter: 'Character One, Character Two',
        filmDuration: 129,
        realiseDate: '2019-04-22',
        startOfRental: '2019-05-10',
        endOfRental: '2019-05-27',
        cover: pondObject
      })
    expect(res.statusCode).toEqual(201)
    expect(res.body.message).toBe('Film added')
    await cinemaService.deleteFilm('Filman Title')
  })

  test('On POST /admin/createfilm should return error if invalid input', async () => {
    const res = await request(app)
      .post('/api/admin/createfilm')
      .send({})
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('"title" is required')
  })

  test('On PUT /admin/updatefilm/:title should update film', async () => {
    const res = await request(app)
      .put('/api/admin/updatefilm/title-film')
      .send({
        screenWriter: 'Writerer',
        filmDuration: 140
      })
    expect(res.statusCode).toEqual(200)
    expect(res.body.message).toBe('Update successfully')
  })

  test('On PUT /admin/updatefilm/:title should return error if invalid input', async () => {
    const res = await request(app)
      .put('/api/admin/updatefilm/title-film')
      .send({
        screenriter: 'Writerer',
        filmDuration: 140
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('"screenriter" is not allowed')
  })

  test('On PUT /admin/updatefilm/:title should return error if fil does not exist', async () => {
    const res = await request(app)
      .put('/api/admin/updatefilm/title-fi')
      .send({
        screenWriter: 'Writerer',
        filmDuration: 140
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('No movie with this title')
  })

  test('On DELETE /admin/delete/:title should delete film', async () => {
    await cinemaService.addFilm(filmBody)

    const res = await request(app)
      .delete('/api/admin/delete/title-film')
    expect(res.statusCode).toEqual(200)
    expect(res.body.message).toBe('Delete successfully')
  })

  test('On DELETE /admin/delete/:title should return error if fil does not exist', async () => {
    const res = await request(app)
      .delete('/api/admin/delete/film-le')
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('No movie with this title')
  })

  test('On GET /films should return all films', async () => {
    const res = await request(app)
      .get('/api/films')
    expect(res.statusCode).toEqual(200)
  })

  test('On GET /film/:title should return all films', async () => {
    const res = await request(app)
      .get('/api/film/title-film')
    expect(res.statusCode).toEqual(200)
  })

  test('On GET /film/:title should return error if title does not exist', async () => {
    const res = await request(app)
      .get('/api/film/film-tit')
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('No movie with this title')
  })
})
