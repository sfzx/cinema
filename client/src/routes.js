import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { MainPage } from './pages/MainPage'
import { AddFilmPage } from './pages/AddFilmPage'
import { RegisterPage } from './pages/RegisterPage'
import { LoginPage } from './pages/LoginPage'
import { UserProfilePage } from './pages/UserProfilePage'

export const useRoutes = isAuthenticated => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path='/cinema/:id' exact>
          <div className='container'>
            <UserProfilePage />
          </div>
        </Route>
        <Route path='/' exact>
          <div className='container'>
            <MainPage />
          </div>
        </Route>
        <Route path='/admin/addfilm' exact>
          <div className='container'>
            <AddFilmPage />
          </div>
        </Route>
        {/* <Redirect to='/' /> */}
      </Switch>
    )
  }
  return (
    <Switch>
      <Route path='/' exact>
        <div className='container'>
          <MainPage />
        </div>
      </Route>
      <Route path='/addfilm' exact>
        <div className='container'>
          <AddFilmPage />
        </div>
      </Route>
      <Route path='/auth/register' exact>
        <div className='auth-body'>
          <RegisterPage />
        </div>
      </Route>
      <Route path='/auth/login' exact>
        <div className='auth-body'>
          <LoginPage />
        </div>
      </Route>
      {/* <Redirect to='/' /> */}
    </Switch>
  )
}
