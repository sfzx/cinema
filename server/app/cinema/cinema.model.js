const { Schema, model } = require('mongoose')

const filmSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  director: {
    type: String,
    required: true
  },
  screenWriter: {
    type: String,
    required: true
  },
  mainCharacter: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  filmDuration: {
    type: Number,
    min: 1,
    required: true
  },
  realiseDate: {
    type: Date,
    required: true
  },
  startOfRental: {
    type: Date,
    required: true
  },
  endOfRental: {
    type: Date,
    required: true
  },
  cover: {
    data: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  }
})

filmSchema.virtual('coverImagePath').get(() => {
  if (this.coverImage != null && this.coverImageType != null) {
    return `data:${this.coverImageType};charset=utf-8;base64,${this.coverImage.toString('base64')}`
  }
})

module.exports = model('Film', filmSchema)
