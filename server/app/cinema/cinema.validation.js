const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'))
const { ErrorHandler } = require('../helpers/error')
const cinemaService = require('./cinema.service')

const validTypes = new RegExp('^image/[A-Za-z]+$')

const createFilm = Joi.object({
  _id: Joi.string(),
  title: Joi.string().min(2).pattern(/^[A-Za-z ]+$/).message('Must include only letter or space').required(),
  director: Joi.string().min(2).pattern(/^[A-Za-z ]+$/).message('Must include only letter or space').required(),
  screenWriter: Joi.string().min(2).pattern(/^[A-Za-z ]+$/).message('Must include only letter or space').required(),
  mainCharacter: Joi.string().min(2).pattern(/^[A-Za-z, ]+$/).message('Must include only letter, space or comma').required(),
  description: Joi.string().required(),
  filmDuration: Joi.number().positive().required(),
  realiseDate: Joi.date().format('YYYY-MM-DD').utc().required(),
  startOfRental: Joi.date().format('YYYY-MM-DD').utc().required(),
  endOfRental: Joi.date().format('YYYY-MM-DD').utc().greater(Joi.ref('startOfRental')).required(),
  cover: Joi.object({ type: Joi.string().pattern(validTypes).required(), data: Joi.string().dataUri().required() }).required()
})

const updateFilm = Joi.object({
  title: Joi.string().min(2).pattern(/^[A-Za-z ]+$/).message('Must include only letter or space'),
  director: Joi.string().min(2).pattern(/^[A-Za-z ]+$/).message('Must include only letter or space'),
  screenWriter: Joi.string().min(2).pattern(/^[A-Za-z ]+$/).message('Must include only letter or space'),
  mainCharacter: Joi.string().min(2).pattern(/^[A-Za-z, ]+$/).message('Must include only letter, space or comma'),
  description: Joi.string(),
  filmDuration: Joi.number().positive(),
  realiseDate: Joi.date().format('YYYY-MM-DD').utc(),
  startOfRental: Joi.date().format('YYYY-MM-DD').utc(),
  endOfRental: Joi.date().format('YYYY-MM-DD').utc().greater(Joi.ref('startOfRental')),
  cover: Joi.object({ type: Joi.string().pattern(validTypes).required(), data: Joi.string().dataUri().required() })
})

async function validateBody (data, schema, next) {
  try {
    const res = await schema.validate(data)
    if (res.error) {
      throw new ErrorHandler(400, res.error.details[0].message)
    }
  } catch (error) {
    next(error)
  }
}

async function validateTitle (title, next) {
  try {
    if (await cinemaService.getFilm(title) === null) {
      throw new ErrorHandler(400, 'No movie with this title')
    }
  } catch (error) {
    next(error)
  }
}

const cinemaValidation = {
  checkBody: async (req, res, next) => {
    await validateBody(req.body, createFilm, next)
    await next()
  },
  checkExistence: async (req, res, next) => {
    await validateTitle(req.params.title, next)
    await next()
  },
  checkUpdateBody: async (req, res, next) => {
    await validateTitle(req.params.title, next)
    await validateBody(req.body, updateFilm, next)
    await next()
  },
  validateDelete: async (req, res, next) => {
    await validateTitle(req.params.title, next)
    await next()
  }
}

module.exports = cinemaValidation
