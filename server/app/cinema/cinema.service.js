const Film = require('./cinema.model')

async function findByTitle (title) {
  const regex = /-/gi
  const correctTitle = new RegExp(['^', title.replace(regex, ' '), '$'].join(''), 'i')
  return await Film.findOne({ title: correctTitle })
}

const cinemaService = {
  addFilm: async (data) => {
    const film = new Film(data)
    return await film.save()
  },
  getAllFilms: async () => {
    return await Film.find({})
  },
  getFilm: async (filmTitle) => {
    return await findByTitle(filmTitle)
  },
  updateFilm: async (data, filmTitle) => {
    const target = await findByTitle(filmTitle)
    return await Film.findOneAndUpdate({ title: target.title }, data)
  },
  deleteFilm: async (filmTitle) => {
    const target = await findByTitle(filmTitle)
    return await Film.deleteOne({ title: target.title })
  }
}

module.exports = cinemaService
