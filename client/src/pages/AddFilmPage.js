import React, { useState, useEffect, useRef } from 'react'
import { useHttp } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
}

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
}

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
}

export const AddFilmPage = () => {
  const message = useMessage()
  const { loading, request, error, clearError } = useHttp()

  const [form, setForm] = useState({
    title: '',
    director: '',
    screenWriter: '',
    filmDuration: '',
    description: '',
    mainCharacter: '',
    realiseDate: '',
    startOfRental: '',
    endOfRental: ''
  })

  const [files, setFiles] = useState([])

  const fileInput = useRef(null)

  const [previewUrl, setPreviewUrl] = useState('')
  const handleFile = file => {
    // you can carry out any file validations here...
    setFiles(file)
    setPreviewUrl(URL.createObjectURL(file))
  }
  const handleOndragOver = event => {
    event.preventDefault()
  }
  const handleOndrop = event => {
    // prevent the browser from opening the image
    event.preventDefault()
    event.stopPropagation()
    // let's grab the image file
    const imageFile = event.dataTransfer.files[0]
    handleFile(imageFile)
  }

  useEffect(() => {
    window.M.updateTextFields()
  }, [])

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const submitHandler = async (e) => {
    const toUint8Array = files => new Promise((resolve, reject) => {
      const reader = new window.FileReader()
      reader.readAsDataURL(files)
      reader.onload = () => resolve(reader.result)
      reader.onerror = error => reject(error)
    })
    const data = {
      ...form,
      cover: {
        type: files.type,
        data: await toUint8Array(files)
      }

    }
    // console.log('data', data)

    try {
      const res = await request('/api/admin/createfilm', 'POST', { ...data })
      message(res.message)
    } catch (e) {}
  }

  return (
    <div>
      <div className='row'>
        <div className='col s12' id='reg-htmlForm'>
          <div className='row'>
            <div className='col s12'>
              <p className='flow-text center-align fonts-styles'>Add new film</p>
            </div>
          </div>
          <div className='row'>
            <div className='drop_zone col s6'>
              <div
                className=''
                onDragOver={handleOndragOver}
                onDrop={handleOndrop}
                onClick={() => fileInput.current.click()}
              >
                <p>Click to select or Drag and drop image here....</p>

                <input
                  id='cover'
                  name='cover'
                  type='file'
                  ref={fileInput} hidden
                  onChange={(e) => handleFile(e.target.files[0])}
                />
              </div>
              {previewUrl &&
                <div style={thumb}>
                  <div style={thumbInner}>
                    <img src={previewUrl} alt='film-preview' style={img} />

                  </div>
                </div>}
            </div>

            <div className='input-field col s6'>
              <input id='title' name='title' type='text' className='validate' value={form.title} onChange={changeHandler} required />
              <label htmlFor='title'>Title</label>
            </div>

            <div className='input-field col s6'>
              <input id='director' name='director' type='text' className='validate' value={form.director} onChange={changeHandler} required />
              <label htmlFor='director'>Directed by</label>
            </div>

            <div className='input-field col s6'>
              <input id='screenWriter' name='screenWriter' type='text' className='validate' value={form.screenWriter} onChange={changeHandler} required />
              <label htmlFor='screenWriter'>Screen writer</label>
            </div>

            <div className='input-field col s6'>
              <input id='filmDuration' name='filmDuration' type='number' min='0' className='validate' value={form.filmDuration} onChange={changeHandler} required />
              <label htmlFor='filmDuration'>Film duration in minutes</label>
            </div>
          </div>

          <div className='row'>
            <div className='input-field col s12'>
              <textarea id='description' name='description' type='text' className='materialize-textarea validate' value={form.description} onChange={changeHandler} required />
              <label htmlFor='description'>Description</label>
            </div>
          </div>

          <div className='row'>
            <div className='input-field col s6'>
              <input id='mainCharacter' name='mainCharacter' type='text' className='validate' value={form.mainCharacter} onChange={changeHandler} required />
              <label htmlFor='mainCharacter'>Main characters</label>
            </div>
            <div className='input-field col s6'>
              <input id='realiseDate' name='realiseDate' type='date' className='validate' value={form.realiseDate} onChange={changeHandler} required />
              <label htmlFor='realiseDate'>Realise date</label>
            </div>
            <div className='input-field col s6'>
              <input id='startOfRental' name='startOfRental' type='date' className='validate' value={form.startOfRental} onChange={changeHandler} required />
              <label htmlFor='startOfRental'>Start of rental</label>
            </div>
            <div className='input-field col s6'>
              <input id='endOfRental' name='endOfRental' type='date' className='validate' value={form.endOfRental} onChange={changeHandler} required />
              <label htmlFor='endOfRental'>End of rental</label>
            </div>
          </div>

          <div className='row'>
            <div className='input-field col s12'>
              <button className='btn btn-large waves-effect waves-light col s12' onClick={submitHandler} disabled={loading}>
                Add Film
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col s12' />
      </div>

    </div>
  )
}
