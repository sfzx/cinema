import React, { useCallback, useState, useEffect } from 'react'
import { useHttp } from '../hooks/http.hook'
import { Loader } from '../components/Loader'
import { FilmCard } from '../components/FilmCard'

export const MainPage = () => {
  const { request, loading } = useHttp()
  // const [image, setImage] = useState(null)
  const [content, setContent] = useState(null)
  const getContent = useCallback(async () => {
    try {
      const fetched = await request('/api/films', 'GET', null)
      console.log('fetched is', fetched)
      setContent(fetched)
      // console.log(fetched)
      // setImage(arrayBufferToBase64(fetched.cover.data, fetched.cover.type))
      // setImage(fetched.cover.data)
    } catch (error) {}
  }, [request])
  console.log('content is', content)

  useEffect(() => {
    getContent()
  }, [getContent])

  if (loading) {
    return <Loader />
  }

  return (
    <div className='row'>
      {!loading && content && content.map(element => <FilmCard content={element} key={element._id} />)}
    </div>
  )
}
