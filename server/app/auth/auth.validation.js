const Joi = require('joi')
const authService = require('./auth.service')
const { ErrorHandler } = require('../helpers/error')

const createUser = Joi.object({
  _id: Joi.string().pattern(new RegExp('/^[0-9a-fA-F]{24}$/')),
  firstName: Joi.string().alphanum().min(2).max(30).required(),
  lastName: Joi.string().alphanum().min(2).max(30).required(),
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }).required(),
  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).required()
})

const loginUser = Joi.object({
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }).required(),
  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).required()
}).required()

async function validateBody (data, schema, next) {
  try {
    const result = await schema.validate(data)

    if (result.error) {
      if (result.error.details[0].context.key === 'firstName') {
        throw new ErrorHandler(400, 'Required field First Name')
      } else if (result.error.details[0].context.key === 'lastName') {
        throw new ErrorHandler(400, 'Required field Last Name')
      } else if (result.error.details[0].context.key === 'password') {
        throw new ErrorHandler(400, 'Password must be minimum 6 characters long')
      }
      throw new ErrorHandler(400, result.error.details[0].message)
    }
  } catch (error) {
    next(error)
  }
}

async function checkIfEmailInUse (email, next) {
  try {
    if (await authService.checkExistence({ email })) {
      throw new ErrorHandler(400, 'Email already in use')
    }
  } catch (error) {
    next(error)
  }
}

async function validateUser (params, next) {
  try {
    if (await authService.checkExistence({ email: params.email })) {
      if (!await authService.comparePassword(params)) {
        throw new ErrorHandler(400, 'Wrong password')
      }
    } else {
      throw new ErrorHandler(400, 'User not found')
    }
  } catch (error) {
    next(error)
  }
}

const validation = {
  register: async (req, res, next) => {
    await validateBody(req.body, createUser, next)
    await checkIfEmailInUse(req.body.email, next)
    await next()
  },
  login: async (req, res, next) => {
    await validateBody(req.body, loginUser, next)
    await validateUser(req.body, next)
    await next()
  }
}

module.exports = validation
