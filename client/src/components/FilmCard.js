import React from 'react'

export const FilmCard = (content) => {
  console.log('FilmCard', content)
  return (
    <div className='col s3'>
      <div className='card'>
        <div className='card-image'>
          <img src={content.content.cover.data} />

          <span className='card-title'><strong>{content.content.title}</strong></span>
        </div>
        <div className='card-content'>
          <p>
            {content.content.description}
          </p>

        </div>
        <p>
            FIlm duration {content.content.filmDuration} min
        </p>
        <div className='card-action'>
          <a href='#'>To the film page</a>
        </div>
      </div>
    </div>
  )
}
