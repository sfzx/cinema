const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const User = require('./auth.user.model')

const authService = {
  checkExistence: async (data) => {
    return await User.findOne(data)
  },
  createUser: async (data) => {
    data.password = await bcrypt.hash(data.password, 12)
    const user = new User(data)
    return await user.save()
  },
  deleteUser: async (data) => {
    return await User.deleteOne(data)
  },
  loginUser: async (data) => {
    const user = await User.findOne({ email: data.email })
    const token = jwt.sign(
      { userId: user.id },
      process.env.JWRSECRET,
      { expiresIn: '1h' }
    )
    return { token, userId: user.id }
  },
  comparePassword: async (data) => {
    const user = await User.findOne({ email: data.email })

    return await bcrypt.compare(data.password, user.password)
  }
}

module.exports = authService
