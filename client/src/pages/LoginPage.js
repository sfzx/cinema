import React, { useState, useEffect, useContext } from 'react'
import MailOutlineOutlined from '@material-ui/icons/MailOutlineOutlined'
import LockOutlined from '@material-ui/icons/LockOutlined'
import { Link } from 'react-router-dom'
import { useMessage } from '../hooks/message.hook'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'

export const LoginPage = () => {
  const auth = useContext(AuthContext)
  const message = useMessage()
  // const history = useHistory()
  const { loading, request, error, clearError } = useHttp()
  const [form, setForm] = useState({
    email: '', password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', { ...form })
      auth.login(data.token, data.userId)
      // history.push('/')
    } catch (e) {}
  }

  return (
    <div id='login-page' className='row animation'>
      <div className='col s12 z-depth-6 card-panel'>
        <div className='login-form'>
          <div className='row'>
            <div className='col s12'>
              <p className='flow-text center-align fonts-styles'>Login</p>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>
              <i className='material-icons prefix'><MailOutlineOutlined fontSize='large' /></i>
              <input id='email' type='email' name='email' value={form.email} onChange={changeHandler} />
              <label htmlFor='email'>Email</label>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>
              <i className='material-icons prefix'><LockOutlined fontSize='large' /></i>
              <input id='password' type='password' name='password' value={form.password} onChange={changeHandler} />
              <label htmlFor='password'>Password</label>
            </div>
          </div>
          <div className='row' />
          <div className='row'>
            <div className='input-field col s12'>
              <button className='btn btn-large waves-effect waves-light col s12' onClick={loginHandler} disabled={loading}>Login</button>
            </div>
          </div>
          <div className='row'>
            <p className='margin medium-small center-align'><Link to='/auth/register'>Register Now!</Link></p>
          </div>

        </div>
      </div>
    </div>
  )
}
