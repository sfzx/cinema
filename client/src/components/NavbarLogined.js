import React, { useState, useContext } from 'react'
import { NavLink, useHistory, Link } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

export const NavbarLogined = () => {
  return (

    <NavBar>

      <NavItem data='some text' />
      <NavItem data='some text' />

      <NavItem data='some text'>
        <DropdownMenu />
      </NavItem>
    </NavBar>
  )
}

const DropdownMenu = () => {
  const DropdownItem = props => {
    return (
      <>
        {props.children}
      </>
    )
  }

  const history = useHistory()
  const auth = useContext(AuthContext)

  const logoudHandler = event => {
    event.preventDefault()
    auth.logout()
    history.push('/')
  }

  return (
    <div className='dropdown'>
      <DropdownItem>
        <NavLink to='/cinema/1'>Profile</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink to='/admin/addfilm'>Add film</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink to='/' onClick={logoudHandler}>logout</NavLink>

      </DropdownItem>
    </div>

  )
}

const NavBar = props => {
  return (
    <nav>
      <Link to='/' className='brand-logo'>Cinema</Link>

      <ul className='right hide-on-med-and-down '>{props.children}</ul>
    </nav>
  )
}

const NavItem = props => {
  const [open, setOpen] = useState(false)

  return (
    <li>

      <Link className='dropdown-trigger' to='#' onClick={() => setOpen(!open)}>
        {props.data}
      </Link>

      {open && props.children}
    </li>
  )
}
