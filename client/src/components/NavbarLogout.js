import React from 'react'
import { NavLink } from 'react-router-dom'

export const NavbarLogout = () => {
  return (
    <div>

      <nav>
        <div className='nav-wrapper'>
          <NavLink to='/' className='brand-logo'>Cinema</NavLink>
          <ul className='right hide-on-med-and-down'>
            <li><NavLink to='/auth/login'>Login</NavLink></li>
            <li><NavLink to='/auth/register'>Register</NavLink></li>

          </ul>
        </div>
      </nav>
    </div>

  )
}
