const { Router } = require('express')
const cinemaService = require('./cinema.service')
const cinemaValidation = require('./cinema.validation')

const router = Router()

// /api/films
router.get('/films', async (req, res) => {
  const films = await cinemaService.getAllFilms()
  res.status(200).json(films)
})

// /api/film/:title
router.get('/film/:title', cinemaValidation.checkExistence, async (req, res) => {
  const film = await cinemaService.getFilm(req.params.title)
  res.status(200).json(film)
})

// /api/admin/createfilm
router.post('/admin/createfilm', cinemaValidation.checkBody, async (req, res) => {
  await cinemaService.addFilm(req.body)
  res.status(201).json({ message: 'Film added' })
})

// /api/admin/updatefilm/:title
router.put('/admin/updatefilm/:title', cinemaValidation.checkUpdateBody, async (req, res) => {
  await cinemaService.updateFilm(req.body, req.params.title)
  res.status(200).json({ message: 'Update successfully' })
})

// /api/admin/updatefilm/:title
router.delete('/admin/delete/:title', cinemaValidation.validateDelete, async (req, res) => {
  await cinemaService.deleteFilm(req.params.title)
  res.status(200).json({ message: 'Delete successfully' })
})

module.exports = router
