/* eslint-env jest */
const app = require('../app')
const request = require('supertest')
const authService = require('../app/auth/auth.service')

const newUser = {
  firstName: 'Fname',
  lastName: 'Lname',
  email: 'new@email.com',
  password: 'asD1234'
}

describe('/api/auth', () => {
  beforeEach(async () => {
    await authService.createUser(newUser)
  })

  afterEach(async () => {
    await authService.deleteUser(newUser)
  })

  test('On POST /login should login user', async () => {
    const res = await request(app)
      .post('/api/auth/login')
      .send({
        email: 'new@email.com',
        password: 'asD1234'
      })
    expect(res.statusCode).toEqual(200)
    expect(res.body.message).toBe('Successful logined')
  })

  test('On POST /login should return error if invalid input', async () => {
    const res = await request(app)
      .post('/api/auth/login')
      .send({
        emal: 'nw@email.com',
        password: 'asD1234'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('"email" is required')
  })

  test('On POST /login should return error if user not found', async () => {
    const res = await request(app)
      .post('/api/auth/login')
      .send({
        email: 'nw@email.com',
        password: 'asD1234'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('User not found')
  })

  test('On POST /login should return error if password not found', async () => {
    const res = await request(app)
      .post('/api/auth/login')
      .send({
        email: 'new@email.com',
        password: 'asD123456'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('Wrong password')
  })

  test('On POST /register shoulde crete new user', async () => {
    const res = await request(app)
      .post('/api/auth/register')
      .send({
        firstName: 'Fname',
        lastName: 'Lname',
        email: 'soe@email.com',
        password: 'asD123'
      })
    expect(res.statusCode).toEqual(201)
    expect(res.body.message).toBe('User created')
    await authService.deleteUser({ email: 'soe@email.com' })
  })

  test('On POST /register shoulde return error if invalid input', async () => {
    const res = await request(app)
      .post('/api/auth/register')
      .send({
        firstName: 'Fname',
        lastName: 'Lname',
        email: 'som',
        password: 'a3'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('"email" must be a valid email')
  })

  test('On POST /register shoulde return error if user with same email already exist', async () => {
    const res = await request(app)
      .post('/api/auth/register')
      .send({
        firstName: 'Fname',
        lastName: 'Lname',
        email: 'new@email.com',
        password: 'asD123'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('Email already in use')
  })
})
